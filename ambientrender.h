#ifndef AMBIENTRENDER_H
#define AMBIENTRENDER_H

#include <QOpenGLExtraFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>
#define PI 3.14159265f
class AmbientRender
{
public:
    AmbientRender() = default;
    void initialize(float r);
    void render(QOpenGLExtraFunctions *f,QMatrix4x4 &projM,QMatrix4x4 & camera,QMatrix4x4 &model);

private:
    QOpenGLShaderProgram m_program;
    QOpenGLBuffer m_vbo;
    QVector<GLfloat> m_points;
    float m_r;
};

#endif // AMBIENTRENDER_H
