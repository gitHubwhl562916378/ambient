#ifndef WIDGET_H
#define WIDGET_H

#include <QOpenGLWidget>
#include "ambientrender.h"

class Widget : public QOpenGLWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = 0);
    ~Widget();

protected:
    void initializeGL() override;
    void paintGL() override;
    void resizeGL(int w, int h) override;
    void wheelEvent(QWheelEvent *event) override;

private:
    AmbientRender m_render;
    QMatrix4x4 m_proMatrix,m_modelMatrix;
    QVector3D m_eye,m_center;
};

#endif // WIDGET_H
