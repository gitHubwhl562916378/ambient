#include "widget.h"
#include <QWheelEvent>

Widget::Widget(QWidget *parent)
    : QOpenGLWidget(parent),
      m_eye(0,0,2),
      m_center(0,0,-1)
{
}

Widget::~Widget()
{

}

void Widget::initializeGL()
{
    m_render.initialize(0.8);
}

void Widget::paintGL()
{
    QOpenGLExtraFunctions *f = QOpenGLContext::currentContext()->extraFunctions();
    f->glClearColor(0.0, 0.0, 0.0, 0.0);
    f->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    m_modelMatrix.rotate(30,0,1,0);

    QMatrix4x4 camera;
    camera.lookAt(m_eye,m_center,QVector3D{0,1,2});

    m_render.render(f,m_proMatrix,camera,m_modelMatrix);
}

void Widget::resizeGL(int w, int h)
{
    m_proMatrix.setToIdentity();
    m_proMatrix.perspective(45,float(w)/h,0.01f,100.0f);
}

void Widget::wheelEvent(QWheelEvent *event)
{
    if (! event->pixelDelta().isNull()) {
        m_eye.setZ(m_eye.z() + event->pixelDelta().y()); //重置视点z值
    } else if (!event->angleDelta().isNull()) {
        m_eye.setZ(m_eye.z() + (event->angleDelta() / 120).y()); //重置视点z值
    }

    event->accept();
    update();
}
