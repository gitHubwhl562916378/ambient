#version 330
uniform mat4 uPMatrix,camMatrix,uMMatrix;
layout (location = 0) in vec3 aPosition;
smooth out vec3 vPosition;
smooth out vec4 vambient;

void main(void)
{
    gl_Position = uPMatrix * camMatrix *uMMatrix * vec4(aPosition,1);
    vPosition = aPosition;
    vambient = vec4(0.30,0.30,0.15,1.0);
}
